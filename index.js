// https://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,

  parserOptions: {
    parser: 'babel-eslint',
  },

  env: {
    node: true,
  },

  extends: [
    'airbnb-base',
    'plugin:vue/recommended',
  ],

  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing':        'off',
    // allow debugger and console during development
    'no-debugger':                       process.env.NODE_ENV === 'production' ? 2 : 0,
    'no-console':                        [process.env.NODE_ENV === 'production' ? 2 : 0, { allow: ['warn', 'error'] }],
    'one-var':                           [2, 'never'],
    'camelcase':                         [2, { 'properties': 'never' }],
    'no-underscore-dangle':              [2, { 'allow': ['_select', '_private', '_agile', '_fbq'] }],
    'space-before-function-paren':       [2, 'always'],
    'semi':                              [2, 'always'],
    'no-new':                            0,
    'no-trailing-spaces':                2,
    'no-whitespace-before-property':     2,
    'space-before-blocks':               2,
    'space-infix-ops':                   2,
    'comma-dangle':                      [2, 'always-multiline'],
    'key-spacing':                       [2, { 'mode': 'minimum' }],
    'no-multi-spaces':                   0,
    'object-curly-spacing':              [2, 'always'],
    'lines-around-directive':            [2, 'always'],
    'newline-before-return':             2,
    'newline-per-chained-call':          [2, { 'ignoreChainWithDepth': 3 }],
    'operator-linebreak':                [2, 'before'],
    'quotes':                            [2, 'single', { 'allowTemplateLiterals': true }],
    'no-unused-vars':                    2,
    'no-unused-expressions':             2,
    'indent':                            0,
    'indent-legacy':                     ['error', 2],
    'no-useless-escape':                 0,
    'padded-blocks':                     0,
    'no-useless-return':                 0,
    'vue/script-indent':                 [0, 2, { 'baseIndent': 1 }],
    'vue/valid-v-for':                   0,
    'vue/require-v-for-key':             2,
    "import/no-extraneous-dependencies": ["error", {"devDependencies": ["**/*.stories.js", "**/*.spec.js"]}],

    // airbnb disabled
    // TODO: Discuss some of them.
    'func-names': 0,
    'import/no-cycle': 0,
    'import/extensions': 0,
    'import/prefer-default-export': 0,
    'max-len': 0,
    'no-alert': 0,
    'no-bitwise': 0,
    'no-plusplus': 0,
    'no-prototype-builtins': 0,
    'no-restricted-globals': 0,
    'no-restricted-syntax': 0,
    'no-underscore-dangle': 0,
    'no-use-before-define': 0,
    'prefer-destructuring': 0,
    'prefer-spread': 0,
    'prefer-rest-params': 0,
    'radix': 0,
    'vue/no-v-html': 0,
    'vue/require-default-prop': 0,
    'vue/require-prop-types': 0,
    'prefer-object-spread': 0,

    /*
      This is important in vue ecosystem. Allows to avoid reacti
    */
    'no-param-reassign': 0,
    /*
      Provides constant arrow function syntax.
    */
    'arrow-parens': 0,
    /*
      This is to allow mocking different values for single unit test
     */
    'global-require': 'off',
  },

  "settings": {
    "import/resolver": {
      "node": {
        "extensions": [".js", ".vue"]
      },
      /*
        TODO: Can be replaced by usage of @vue/airbnb when vsc update runtime node for eslint
        because now eslint in vsc cannot use custom aliases like `require.resolve('...')` used in vue-cli :/
        now eslint have to duplicate aliases config.
      */
      alias: {
        map: [
          ['router', './src/js/router'],
          ['main-styles', './src/styles/semantic/semantic.less'],
          ['@', './src'],
          ['vue$', 'vue/dist/vue.runtime.esm.js'],
        ],
        extensions: ['.js', '.json', '.vue']
      }
    },
  },

  overrides: [
    {
      files: [
        '**/src/**/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      },
    },
  ]
};
