# Company wide ESLint rules

## How to use?
In order to use those eslint rules you need to do the following:

```javascript
yarn add https://gitlab.com/piotr22/eslint-config-appjobs.git#<tag> --dev
yarn install
```

or if you prefer just add this line to `devDependencies` in `package.json` and run `yarn install`.

Remember to replace the `<tag>` with current tag version (eg. `1.0.4`).

Then modify the `.eslintrc.js` file to something like this:

```javascript
module.exports = {
  extends: '@appjobs/eslint-config-appjobs',

  // The following is just addition to override/add some project specifics
  // and you don't have to have it at all.
  globals: {
    'cloudinary': true,
    'prePopulated': true,
    'Nofifier': false,
  },
};
```

## How to contribute?
* make changes in the eslint config file (`index.js`)
* push changes (or do it with MR if really needed)
* bump version and push
* make tag and push

  `git tag -a <version> -m"<message>"`
  
  * `<version>` - version number set in package.json
  * `<message>` - a message about the tag (required)
  
    Example: `git tag -a 1.0.4 -m"Some fancy rule added"`

## What happens on tag?

The notification is sent to team-member-front slack channel. This is where every team should take an action and update the version in their project's package.json.
